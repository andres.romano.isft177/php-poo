<?php 
/**
 * Documentacion oficial de PHP-POO
 * 
 * @link https://www.php.net/language.oop5
 * 
 * Una clase es el conjunto de metodos y propiedades que forman un objeto
 */

# Crear una clase, se utiliza la palabra reservada class NombreClase {}

class Gato {

}

# Verificar si una clase existe

echo class_exists("Gato") ? "la clase existe" : "la NO clase existe";
