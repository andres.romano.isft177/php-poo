<?php
/*
Otra pieza fundamental de la programación orientada a objetos en 
PHP (en cualquier lenguaje OOP) es la creación de los métodos, 
que no son otra cosa mas que nuestras conocidas funciones
*/
class Gato {
    function maullar() {
        echo "El gato dice miau miau"."<br>";
    }
    function ronronear() {
        echo "El gato ronronea"."<br>";
    }
}

# obtener los metodos de una clase

$metodos = get_class_methods("Gato");
foreach ($metodos as $metodo) {
    echo $metodo."<br>";
}

# verificar si existe un metodo

if (method_exists("Gato", "maullar")) {
    echo "Los gatos pueden maullar"."<br>";
} else {
    echo "Los gatos NO pueden maullar"."<br>";
}
