<?php 
/*
  Una instancia es crear un objeto que podemos utilizar en 
  base a la clase que hemos definido, por lo que tendrá los 
  mismos métodos y propiedades iniciales que definimos en 
  nuestro archivo de clase.
*/

class Gato {

	function maullar(){
		return "miau, miau";
	}

}

# Creamos las instancias

$cucho = new Gato();
$benito = new Gato();
$espanto = new Gato();

# detectar la clase de una instancia / objeto

echo "Espanto pertenece a la clase ".get_class($espanto)."<br>";

# Verificar que un objeto pertenezca a una clase

echo "Matute ";
if (is_a($matute,"Gato")) { // is_a() comprueba si es un objeto
	echo "Si es un gato"."<br>";
} else {
	echo "No es un gato"."<br>";
}

# Llamar a un metodo

echo "Cucho dice ".$cucho->maullar()."<br>";
echo "Benito dice ".$benito->maullar()."<br>";
echo "Espanto dice ".$espanto->maullar()."<br>";