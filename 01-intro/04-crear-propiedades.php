<?php 
/*
Las propiedades las podemos ubicar como lo que le 
es "propio" a la instancia. No son otra cosa que 
variables que definen al objeto y que son establecidas 
en el archivo de clase. 
*/

class Gato {
	# propiedades
	var $nombre;
	var $colorPelo;
	var $corbata = "SI";

	# metodos
	function maullar(){
		return "miau, miau";
	}

	function tieneCorbata(){
		return $this->nombre." ".$this->corbata." tiene corbata y su color de pelo es ".$this->colorPelo."<br>";
	}

}
# instancias
$cucho = new Gato();

# Poblar las propiedades
$cucho->nombre = "Cucho";
$cucho->colorPelo = "rosa";
$cucho->corbata = "NO";

echo $cucho->nombre." dice ".$cucho->maullar()."<br>";
echo $cucho->tieneCorbata();
