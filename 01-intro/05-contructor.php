<?php 
/*
Un constructor es la función que se ejecuta cuando el objeto 
es creado o instanciado, y nos servirá para recibir parámetros 
iniciales o para ejecutar acciones o métodos cuando el objeto
es "construido".
*/

class Gato {
	var $nombre;
	var $colorPelo;
	var $corbata = "SI";
	
	function __construct($nombre="", $pelo="negro"){
		$this->nombre = $nombre;
		$this->colorPelo = $pelo;
	}

	function maullar(){
		return "miau, miau";
	}

	function tieneCorbata(){
		return $this->nombre." ".$this->corbata." tiene corbata y su color de pelo es ".$this->colorPelo."<br>";
	}

	function saludo(){
		$cadena = "Hola, me llamo ".$this->nombre." y mi color de pelo es ";
		$cadena .= $this->colorPelo;
		return $cadena;
	}
}
# instancias
$cucho = new Gato("Cucho","rosa");

echo $cucho->nombre." dice ".$cucho->maullar()."<br>";
echo $cucho->tieneCorbata();
echo $cucho->saludo();

$benito = new Gato();
echo "<br>".$benito->colorPelo;