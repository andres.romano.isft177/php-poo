<?php 
/*
  Herencia, se da cuando una clase hereda todas las propiedades y metodos de otra
  El valor de la constante es la misma para todas las instancias, no se pueden modificar.
  El valor de una constante no puede ser una variable, función o expresión.
  Por omisión, las variables de clase son públicas.
  Las constantes se diferencian de las variables en que no es necesario el símbolo de pesos
  o dolar para definirlas ni para utilizarlas.

  Para esto debemos utilizar la palabra reservada extends
*/

class Gato {
	var $nombre;
	var $colorPelo;
	var $corbata = "SI";
	function __construct($nombre="", $pelo="negro"){
		$this->nombre = $nombre;
		$this->colorPelo = $pelo;
	}
	function maullar(){
		return "miau, miau";
	}
	function tieneCorbata(){
		return $this->nombre." ".$this->corbata." tiene corbata y su color de pelo es ".$this->colorPelo."<br>";
	}
	function saludo(){
		$cadena = "Hola, soy ".$this->nombre." y mi color de pelo es ";
		$cadena .= $this->colorPelo;
		return $cadena;
	}
}

class OtroGato extends Gato{
	public $edad;
}

$cucho = new Gato("Cucho", "rosa");
$benito = new OtroGato("Benito","azul");

echo $cucho->saludo()."<br>";
echo $benito->saludo()."<br>";

unset($cucho);
unset($benito);

echo "El pariente de la clase Gato es ".get_parent_class("Gato")."<br>";
echo "El pariente de la clase OtroGato es ".get_parent_class("OtroGato")."<br>";
echo "<br>";
echo is_subclass_of("Gato", "OtroGato") ? "Si" : "No";
echo "<br>";
echo is_subclass_of("OtroGato", "Gato") ? "Si" : "No";
echo "<br>";
