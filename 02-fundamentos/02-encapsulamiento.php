<?php 
/*
Otro de los conceptos básicos de la programación orientada a objetos es el encapsulamiento,
con lo cual el usuario (otro programador u otro código) sólo podrá ver aquello que nosotros
deseemos y lo restante estará "encapsulado" o restringido.
Los modificadores de acceso son el pilar en el que se basa el encapsulamiento, ya que con
estos modificadores permitimos que el "usuario" vea o no las propiedades y métodos de
nuestras clases.
Modificadores de acceso:

 public: acceso al recurso. Valor por omisión.
 private: sólo tiene acceso dentro de la clase.
 protected: acceso sólo dentro de la clase y de las clases heredadas.
*/

class Gato {

	protected $nombre;
	private $colorPelo;
	private $corbata = "SI";

	public function __construct($nombre="", $pelo="negro"){
		$this->nombre = $nombre;
		$this->colorPelo = $pelo;
	}

	public function maullar() {
		return "miau, miau";
	}

	public function tieneCorbata() {
		return $this->nombre." ".$this->corbata." tiene corbata y su color de pelo es ".$this->colorPelo."<br>";
	}

	public function saludo() {
		$cadena = "Hola, soy ".$this->nombre." y mi color de pelo es ";
		$cadena .= $this->colorPelo;
		return $cadena;
	}

}

class OtroGato extends Gato {

	public function nombreOtroGato() {
		return $this->nombre;
	}

}

$cucho = new Gato("Cucho", "rosa");
$benito = new OtroGato("Benito","azul");

echo $cucho->saludo()."<br>";
echo $benito->saludo()."<br>";
echo "El nombre del otro gato es: ".$benito->nombreOtroGato()."<br>";