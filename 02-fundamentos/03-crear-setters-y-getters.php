<?php 
/*
Otra herramienta que nos permitirá manejar el encapsulamiento de 
nuestras clases es realizar las funciones "getters" y "setters".
Estas son requeridas para que el usuario no acceda directamente 
a nuestras propiedades
*/
class Gato {

	protected $nombre;
	private $colorPelo;
	private $corbata = "SI";

	public function __construct($nombre="", $pelo="negro"){
		$this->nombre = $nombre;
		$this->colorPelo = $pelo;
	}

	public function setCorbata($c="SI"){
		if($c!="SI"){
			$corbata = "NO";
		}
		$this->corbata = $c;
	}
	public function getCorbata(){
		return $this->corbata;
	}

	public function maullar() {
		return "miau, miau";
	}

	public function tieneCorbata() {
		return $this->nombre." ".$this->corbata." tiene corbata y su color de pelo es ".$this->colorPelo."<br>";
	}

	public function saludo() {
		$cadena = "Hola, soy ".$this->nombre." y mi color de pelo es ";
		$cadena .= $this->colorPelo;
		return $cadena;
	}

}

class OtroGato extends Gato {

	public function nombreOtroGato() {
		return $this->nombre;
	}

}

$cucho = new Gato("Cucho", "rosa");
$benito = new OtroGato("Benito","azul");

echo $cucho->saludo()."<br>";
echo $benito->saludo()."<br>";
echo "El nombre del otro gato es: ".$benito->nombreOtroGato()."<br>";

$cucho->setCorbata("NO");

echo $cucho->tieneCorbata();
echo $benito->tieneCorbata();
