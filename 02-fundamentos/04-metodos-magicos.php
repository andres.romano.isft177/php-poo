<?php 
/*
Otros de los llamados "métodos mágicos" son los __get y __set, 
que curiosamente NO sirven para hacer getters y setters.

__set() se ejecuta al escribir datos sobre propiedades inaccesibles.
__get() se utiliza para consultar datos a partir de propiedades inaccesibles
*/
class Gato {

	protected $nombre;
	private $colorPelo;
	private $corbata = "SI";

	public function __construct($nombre="", $pelo="negro") {
		$this->nombre = $nombre;
		$this->colorPelo = $pelo;
	}

	function __set($prop, $value) {

		echo "La propiedad ".$prop." se actualizo a ".$value."<br>";
		if($prop == "corbata"){
			if($value!="SI") $value = "NO";
		}
		$this->$prop = $value;

	}
	function __get($prop){
		return $this->$prop;
	}

	function setCorbata($c="SI"){
		if($c!="SI"){
			$corbata = "NO";
		}
		$this->corbata = $c;
	}
	function getCorbata(){
		return $this->corbata;
	}

	function maullar() {
		return "miau, miau";
	}

	function tieneCorbata() {
		return $this->nombre." ".$this->corbata." tiene corbata y su color de pelo es ".$this->colorPelo."<br>";
	}

	function saludo() {
		$cadena = "Hola, soy ".$this->nombre." y mi color de pelo es ";
		$cadena .= $this->colorPelo;
		return $cadena;
	}

}

class OtroGato extends Gato {

	function nombreOtroGato() {
		return $this->nombre;
	}

}

$cucho = new Gato("Cucho", "rosa");
$benito = new OtroGato("Benito","azul");

echo $cucho->saludo()."<br>";
echo $benito->saludo()."<br>";
echo "El nombre del otro gato es: ".$benito->nombreOtroGato()."<br>";

//$cucho->setCorbata("NO");

$cucho->corbata = "NO";

echo $cucho->tieneCorbata();
echo $benito->tieneCorbata();
