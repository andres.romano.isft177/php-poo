<?php 
/*
Métodos y propiedades estáticas
Un modificador de acceso de mucha utilidad es "static", que 
nos permitirá utilizar métodos y propiedades sin necesidad de 
crear instancias de la clase.
El Operador de Resolución de Ámbito o el doble dos-puntos (::), 
es un operador que permite acceder a elementos estáticos, 
constantes, y sobrescribir propiedades o métodos de una clase.
*/
class Gato {
	static public $claveSecreta = "12345";
	protected $nombre;
	private $colorPelo;
	private $corbata = "SI";

	public function __construct($nombre="", $pelo="negro") {
		$this->nombre = $nombre;
		$this->colorPelo = $pelo;
	}

	public function __set($prop, $value) {

		echo "La propiedad ".$prop." se actualizo a ".$value."<br>";
		if($prop == "corbata") {
			if($value!="SI") $value = "NO";
		}
		$this->$prop = $value;

	}
	public function __get($prop) {
		return $this->$prop;
	}

	static public function mensajeSecreto() {
		return "Hola soy el mensaje secreto!!";
	}

	public function setCorbata($c="SI") {
		if($c!="SI"){
			$corbata = "NO";
		}
		$this->corbata = $c;
	}

	public function getCorbata() {
		return $this->corbata;
	}

	public function maullar() {
		return "miau, miau";
	}

	public function tieneCorbata() {
		return $this->nombre." ".$this->corbata." tiene corbata y su color de pelo es ".$this->colorPelo."<br>";
	}

	public function saludo() {
		$cadena = "Hola, soy ".$this->nombre." y mi color de pelo es ";
		$cadena .= $this->colorPelo;
		return $cadena;
	}

}

class OtroGato extends Gato {

	public function nombreOtroGato() {
		return $this->nombre;
	}

}

echo "La clave secreta es: ".Gato::$claveSecreta."<br>";
echo "La frase secreta es: ".Gato::mensajeSecreto()."<br>";
