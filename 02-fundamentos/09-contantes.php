<?php 
/**
 * Constantes en clases
 * Las constantes se diferencian de las variables en que no 
 * es necesario el símbolo de pesos para definirlas ni para utilizarlas.
 * Por omisión, las variables de clase son públicas.
 * El valor de una constante no puede ser una variable, función o expresión.
 * El valor de la constante es la misma para todas las instancias, 
 * no se pueden modificar.
 */

class MiClase {

	const CONSTANTE = 'valor constante';

	function mostrarConstante() {
		echo self::CONSTANTE."<br>";
	}

}

echo MiClase::CONSTANTE."<br>";

$nombreclase = "MiClase";

echo $nombreclase::CONSTANTE."<br>";

$clase = new MiClase();
$clase->mostrarConstante();

echo $clase::CONSTANTE."<br>"; 