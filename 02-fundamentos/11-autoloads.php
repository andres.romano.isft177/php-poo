<?php 
/**
 * Crear un autoload
 * Con la función spl_autoload_register() podemos autocargar 
 * las clases que llamemos en nuestro programa.
 */

spl_autoload_register(function($class){
	require_once './classes/'.$class.'.php';
});

$gato = new Gato();
$auto = new Auto();
$frutas = new Frutas();
$otro_gato = new OtroGato();

$auto->modelo = "Honda";
echo $auto->modelo();
