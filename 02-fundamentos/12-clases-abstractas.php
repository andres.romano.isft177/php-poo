<?php 
/**
 * Clases Abstractas:
 * 
 * Generalmente se utilizan en aplicaiones avanzadas, cuando hay una jerarquia
 * de clases grande, las clases abstractas estan pensadas para ser heredadas
 * 
 * Las clases abstractas no pueden ser instanciadas.
 * Pueden declarar la existencia de los métodos, pero no su implementación 
 * ,osea ,escribir el codigo de lo que hace.
 * Puede contener, tambien, métodos no-abstractos.
 * Los métodos abstractos deben ser definidos en las clases heredadas,
 * pero deben tener la misma visibilidad (public, private o protected).
 * La implementación debe tener la misma “firma”: la declaración 
 * de tipos y el número de argumentos.
 * 
 */

abstract class Mamifero{
//Metodo Abstracto
	abstract public function saludo();
//Método no-abstracto
	public function maullar(){
		return "miau, miau";
	}
}
class Gato extends Mamifero{

	public function saludo(){
		return "Hola Mundo";
	}
	
}
//$mam = new Mamifero();
$gato = new Gato();
echo "saludo ".$gato->saludo()."<br>";
echo "Maullar ".$gato->maullar()."<br>";