<?php 
/**
 * Introducción a los espacio de nombres o namespace.
 * 
 * Los Espacios de nombres o namespaces son una forma de “encapsular” 
 * elementos de tal manera de distinguirlos de otros elementos, que 
 * podrían llamarse igual.
 * Una analogía podría ser “apellidos”: los archivos se llaman “juan.txt” 
 * pero uno es “perez” y el otro “lopez”. 
 * Una mejor analogía es el sistemas de archivos de un disco duro.
 * Los espacios de nombres nos permiten agrupar clases, interfaces, 
 * funciones y constantes relacionadas por lo general en una librería.
 * En PHP se utilizan los espacios de nombres para la creación de librerías, 
 * evitando el conflicto de nombre de funciones o variables, y para simplificar 
 * nombres de elementos muy largos, que tratan de evitar el primer problema en 
 * el desarrollo de código reutilizable.
 * 
 * Nota: Los nombres de los espacios de nombres PHP y php, y los nombres 
 * compuestos a partir de estos (como PHP\Clases) están reservados para el 
 * uso interno del lenguaje y no deben utilizarse en el código del espacio 
 * del usuario.
 * 
 * Aunque cualquier código de PHP válido puede estar contenido dentro de un 
 * espacio de nombres, solamente se ven afectados por espacios de nombres los 
 * siguientes tipos de código: clases (incluyendo abstractas), interfaces, 
 * funciones y constantes.
 * Los espacios de nombres se pueden definir en diferentes archivos.
 * Los espacios de nombres se definen con la palabra reservada namespace que 
 * debe de definirse antes de cualquier sentencia (incluyendo espacios en blanco), 
 * excepto de las sentencia “declare”.
 */
namespace Perro;

const NOMBRE = "Lazzy";

class MiPerro { 
	/*...*/
}

function pasearAlPerro() { 
	/*******/ 
}