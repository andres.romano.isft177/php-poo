<?php 
/**
 * Definir varios espacios de nombres
 * 
 * Podemos definir varios espacios de nombre dentro de un mismo archivo 
 * bajo la sintaxis de llaves.
 * Sólo se permiten las sentencias “declare” fuera de las llaves.
 */
<?php
namespace Perro {

	const NOMBRE = "Lazzy";
	class Comer {
		/************/
	}

	function pasear() {
		/***********/
	}
}

namespace Gato {

	const NOMBRE = "Garfield";

	class Comer {
		/************/
	}

	function pasear() {
		/***********/
	}
}