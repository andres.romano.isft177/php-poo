<?php 
/**
 * Como se ve en el ejemplo asignando un "string" con el nombre
 * de un namespace, clase, funcion o constante, se pueden utilizar
 * desde una variable
 */
namespace miNamespace;

class miClase{
	function __construct(){
		echo "Hola desde la funcion constructora<br>";
	}
}

function miFuncion(){
	echo "Hola desde la función<br>";
}

const miConstante = "Hola<br>";

//dinamica
$a = "\miNamespace\miClase";
$clase = new $a;
$b = "\miNamespace\miFuncion";
$b();
echo constant("\miNamespace\miConstante");