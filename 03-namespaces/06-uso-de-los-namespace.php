<?php 
/**
 * Uso de los espacios de nombres: el comando use
 * 
 * El comando "use" nos permite referirnos a un nombre completamente 
 * cualificado externo con un alias. Esto es similar a los sistemas de 
 * ficheros basados en un enlace simbólico a un archivo o directorio.
 * 
 * También permite apodar o importar nombres de funciones y constantes.
 * 
 * Las clases, funciones y constantes que se importen desde el mismo 
 * namespace ahora pueden ser agrupadas en una única sentencia use.
 */
require "use.php";
/*
use Animales\Mamiferos\{ Perro as MiPerro, Gato };
use function Animales\Mamiferos\{ladrar as ladrido, maullar};
use const Animales\Mamiferos\{PERRO as DOG, GATO as CAT };
*/
use Animales\Mamiferos\Perro as MiPerro;
use Animales\Mamiferos\Gato;
use function Animales\Mamiferos\ladrar as ladrido;
use function Animales\Mamiferos\maullar;
use const Animales\Mamiferos\PERRO as DOG;
use const Animales\Mamiferos\GATO;

echo "<h2>Clases del espacio de nombres</h2>";

$perro = new MiPerro;
$gato = new Gato;

echo "<h2>Funciones del espacio de nombres</h2>";

ladrido();
maullar();

echo "<h2>Constantes del espacio de nombres</h2>";

echo DOG."<br>";
echo GATO."<br>";
