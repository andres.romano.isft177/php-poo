<?php

namespace Animales\Mamiferos;

class Perro{

	function __construct(){
		echo "Hola, soy un perro<br>";
	}
}

class Gato{
	function __construct(){
		echo "Hola, soy un gato<br>";

	}
}

function ladrar(){ echo "Guau, guau<br>"; }

function maullar(){ echo "Miau, miau<br>"; }

const PERRO = "Lazzy";

const GATO = "Gardfield";
