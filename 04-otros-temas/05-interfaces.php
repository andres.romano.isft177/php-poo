<?php 
/**
 * Interfaces
 * 
 * Las interfaces nos permiten crear código con el cual especificar qué 
 * métodos deben ser implementados por una clase (prototipos), sin 
 * definir su implementación.
 * Las interfaces se definen con la palabra reservada interface.
 * Los métodos declarados en una interfaz deben ser públicos.
 * Para implementar una interface desde una clase se utiliza la
 * palabra reservada implements.
 * Una clase puede implementar más de una interface si se desea.
 * Al igual que las clases abstractas, se van a utilizar en sistemas muy grandes
 * Como buena practica el nombre debe empezar con "i"
 * 
 */
interface iMamifero{
	public function andar();
	
}
class Gato implements iMamifero{
	public function andar(){
		return "camina";
	}
	
}

class Delfin implements iMamifero{
	public function andar(){
		return "nada";
	}
	
}

class Murcielago implements iMamifero{
	public function andar(){
		return "vuela";
	}

}

$gato = new Gato();
$delfin = new Delfin();
$murcielago = new Murcielago();

echo "El gato ".$gato->andar()."<br>";
echo "El delfin ".$delfin->andar()."<br>";
echo "El murcielago ".$murcielago->andar()."<br>";