<?php 
/**
 * Iteración de objetos
 * 
 * Podemos iterar las propiedades visibles o públicas de una clase.
 * 
 */
class Gato{

	public $peso = "publico";
	private $genero = "private";
	protected $edad = "protegido";

	function iterar(){
		echo "Iteracion dentro de la clase"."<br>";
		foreach ($this as $key => $value) {
			echo $key." -> ".$value."<br>";
		}
	}

}

$gato = new Gato();

$gato->iterar();

echo "<br>"."Iteracion fuera de la clase"."<br>";
foreach ($gato as $key => $value) {
	echo $key." -> ".$value."<br>";
}
