<?php 
/**
 * Evitar la sobreescritura
 * 
 * Una clase también puede ser final y no se permitiría que se 
 * extendiera o heredara a otra clase.
 * Por medio de la palabra reservada final evitamos que la clase
 * derivada sobreescriba un método.
 */
#final class Gato{
class Gato{
	public function ronronear(){
		return "ronronear";
	}
	final function maullar(){
		return "miau, miau";
	}
}

class OtroGato extends Gato{
/*
 	public function maullar(){
 		return "miauuuuu, miauuuuu";
	}
*/
	public function ronronear(){
		return "ron, ron, ron";
	}

}

$otro_gato = new OtroGato();

echo "Maullar: ".$otro_gato->maullar()."<br>";
echo "Ronronear: ".$otro_gato->ronronear()."<br>";