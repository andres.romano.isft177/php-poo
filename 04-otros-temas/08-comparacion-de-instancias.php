<?php 
/**
 * Comparación de instancias
 * 
 * Dos instancias de una clase son iguales si tienen los mismos 
 * atributos y valores (los valores se comparan con el operador de igualdad ==).
 * Cuando se utiliza el operador identidad (===), las variables 
 * de un objeto son idénticas sí y sólo sí hacen referencia a la 
 * misma instancia de la misma clase.
 */
class Gato{
	public $bandera;
}
class Perro{
	public $bandera;
}

$gato1 = new Gato();
$gato2 = new Gato();
$gato3 = $gato1;
$perro1 = new Perro();

echo "Comparamos elementos de la misma clase"."<br>";
echo "gato1 == gato2 =>";
echo ($gato1==$gato2)?"Verdadero":"Falso";
echo "<br>";

echo "gato1 != gato2 =>";
echo ($gato1!=$gato2)?"Verdadero":"Falso";
echo "<br>";

echo "gato1 === gato2 =>";
echo ($gato1===$gato2)?"Verdadero":"Falso";
echo "<br>";

echo "gato1 !== gato2 =>";
echo ($gato1!==$gato2)?"Verdadero":"Falso";
echo "<br>";

echo "<br>";
echo "Comparamos elementos de la misma clase a la misma referencia"."<br>";
echo "gato1 == gato3 =>";
echo ($gato1==$gato3)?"Verdadero":"Falso";
echo "<br>";

echo "gato1 != gato3 =>";
echo ($gato1!=$gato3)?"Verdadero":"Falso";
echo "<br>";
echo "gato1 === gato3 =>";
echo ($gato1===$gato3)?"Verdadero":"Falso";
echo "<br>";

echo "gato1 !== gato3 =>";
echo ($gato1!==$gato3)?"Verdadero":"Falso";
echo "<br>";

echo "<br>";
echo "Comparamos elementos de la diferente clase"."<br>";
echo "gato1 == perro1 =>";
echo ($gato1==$perro1)?"Verdadero":"Falso";
echo "<br>";

echo "gato1 != perro1 =>";
echo ($gato1!=$perro1)?"Verdadero":"Falso";
echo "<br>";

echo "gato1 === perro1 =>";
echo ($gato1===$perro1)?"Verdadero":"Falso";
echo "<br>";

echo "gato1 !== perro1 =>";
echo ($gato1!==$perro1)?"Verdadero":"Falso";
echo "<br>";