<?php 
/**
 * Clases anónimas
 * 
 * Las clases anónimas son útiles para definir objetos sencillos y “desechables”.
 * Generalmente deben tener poco codigo, sino no se justifica y es preferible
 * crear una clase de forma normal
 * 
 */
$perro = new Class(){

	public function getNombre(){
		return "Hola soy un Perro"."<br>";
	}

};
$gato = new Class("Don Gato"){

	private $nombre;

	public function __construct($nombre){
		$this->nombre = $nombre;
	}

	public function getNombre(){
		return "Mi nombre es ".$this->nombre."<br>";
	}

};

echo $gato->getNombre();

echo $perro->getNombre();
