<?php 
/**
 * Metodo __toString()
 * 
 * Se lo considera un metodo magico, es para tener un conversion de clase
 * a cadena, si quisieramos sacar por pantalla el nombre de una instancia 
 * por medio de un echo nos daria error
 */
class Gato {

  private $nombre;
  private $pelo;

  public function __construct($nombre, $color) {

    $this->nombre = $nombre;
    $this->pelo = $color;
  }
/*
  public function __toString() {

    return "Mi nombre es ".$this->nombre." y el color de mi pelo es ".$this->pelo.".<br>";
  }
*/
}

$benito = new Gato("Benito","azul");

echo $benito;