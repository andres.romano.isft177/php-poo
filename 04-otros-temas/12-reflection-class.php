<?php 
/**
 * Con Reflection Class podemos obtener informacion sobre
 * una clase en particular que necesitamos
 */
class Gato{
	public function ronronear(){
		return "ronronear";
	}
	final function maullar(){
		return "miau, miau";
	}
}

class OtroGato extends Gato{

	public function ronronear(){
		return "ron, ron, ron";
	}

}

echo "<pre>";
$info = new ReflectionClass("OtroGato");
Reflection::export($info);
echo "</pre>";